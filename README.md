# An amazing name

What am I?

[![pipeline status](https://gitlab.com/CHANGE/ME/badges/main/pipeline.svg)](https://gitlab.com/CHANGE/ME/-/commits/main) [![coverage report](https://gitlab.com/CHANGE/ME/badges/main/coverage.svg)](https://gitlab.com/CHANGE/ME/-/commits/main) [![Latest Release](https://gitlab.com/CHANGE/ME/-/badges/release.svg)](https://gitlab.com/CHANGE/ME/-/releases)

&nbsp;

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)

&nbsp;

## Installation

How can you install me?

&nbsp;

## Usage

How can you use me?
